package it.prismaprogetti.academy.configuratoreprodotto;

import java.io.IOException;

import it.prismaprogetti.academy.configuratoreprodotto.model.Record;

public interface Reader {
	
	public Record[] read(String path) throws IOException;

}
