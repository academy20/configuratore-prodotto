package it.prismaprogetti.academy.configuratoreprodotto.run;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import it.prismaprogetti.academy.configuratoreprodotto.Reader;
import it.prismaprogetti.academy.configuratoreprodotto.Writer;
import it.prismaprogetti.academy.configuratoreprodotto.impl.ReaderImpl;
import it.prismaprogetti.academy.configuratoreprodotto.impl.WriterImpl;
import it.prismaprogetti.academy.configuratoreprodotto.model.Campo;
import it.prismaprogetti.academy.configuratoreprodotto.model.Colonna;
import it.prismaprogetti.academy.configuratoreprodotto.model.Record;

public class Main {

	public static void main(String[] args) {

		String path = "C:\\Users\\patri\\eclipse-workspace\\configuratore-prodotto\\src\\main\\resources\\data.txt";

		Scanner in = new Scanner(System.in);
		int numeroColonne = 0;
		int numeroDiRecord = 0;
		int numeroDiCampi = 0;
		List<Colonna> colonne = new ArrayList<>();
		List<Campo> campiDelRecord = new ArrayList<>();
		List<Record> records = new ArrayList<>();
		boolean acquisizioneDatiCompletataConSuccesso = false;

		try {

			/*
			 * Richiesta numero colonne
			 */
			System.out.println("Inserisci il numero di colonne");
			numeroColonne = in.nextInt(); //INPUT DA tastiera
			if (numeroColonne < 0) {
				throw new NumberFormatException("numero colonne non valido");
			}

			/*
			 * inserisci il numero di record che vuoi inserire e ricorda ad ogni record sono associati
			 * uno o + campi
			 */
			System.out.println("Inserisci il numero di record");
			numeroDiRecord = in.nextInt();
			if (numeroColonne < 0) {
				throw new NumberFormatException("numero di record non valido");
			}

			/*
			 * Creazione delle colonne
			 */
			for (int i = 0; i < numeroColonne; i++) {

				System.out.println("Inserisci il nome della colonna numero " + (i + 1) + " di " + numeroColonne);
				Colonna colonna = Colonna.createOrNullString(in.next());
				colonne.add(colonna);
			}

			/*
			 * gestione dei records
			 */
			for (int j = 0; j < numeroDiRecord; j++) {

				/*
				 * Richiesta numero campi da associare al record corrente: il numero dei campi
				 * non pu� eccedere il numero delle colonne
				 */
				System.out.println("Inserisci il numero di campi associato al record numero " + (j + 1) + " di "
						+ numeroDiRecord + " che non deve essere maggiore di " + numeroColonne);
				numeroDiCampi = in.nextInt();
				if (numeroDiCampi < 0 || numeroDiCampi > numeroColonne) {
					throw new NumberFormatException(
							"Numero di campi non valido inserire un numero maggiore di zero e minore del numero di colonne desiderate: "
									+ numeroColonne);
				}

				/*
				 * gestione campi del record corrente
				 */
				for (int k = 0; k < numeroDiCampi; k++) {

					/*
					 * Creazione dei campi
					 */
					System.out.println("Inserisci il nome del campo numero " + (k + 1) + " di " + numeroDiCampi
							+ " del record " + (j + 1) + " di " + numeroDiRecord);
					Campo campo = Campo.createOrNullString(in.next());
					campiDelRecord.add(campo);
				}

				/*
				 * gestisco i campi <<NULL>>
				 */
				for (int u = 0; u < (numeroColonne - numeroDiCampi); u++) {

					System.out.println("Inserisco <<NULL>> nei campi mancanti del record");
					Campo campoNull = Campo.createCampoNull();
					campiDelRecord.add(campoNull);
				}

				/*
				 * Creo il Record
				 */
				Record record = new Record("record" + (j + 1));

				/*
				 * assoccio i campi al record
				 */
				for (Campo campo : campiDelRecord) {
					record.getCampiAssociati().add(campo);
				}
				campiDelRecord.clear();

				/*
				 * aggiungo il record creato con i rispettivi campi a una lista
				 */
				records.add(record);

				acquisizioneDatiCompletataConSuccesso = true;

			}

		} catch (InputMismatchException e) {
			System.out.println("input non valido perch� il valore risulta::" + e.getMessage());
		} finally {
			in.close();
		}

		/*
		 * Una volta che ho creato le colonne e i records con i campi associati scrivo
		 * questi su un file txt
		 */
		if (acquisizioneDatiCompletataConSuccesso) {

			Writer writer = new WriterImpl('*');
			Reader reader = new ReaderImpl('*');

			try {
				writer.write(colonne, records, path);
				Record[] recordsArray = reader.read(path);
				
				for ( Record record : recordsArray) {
					System.out.println(record);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
