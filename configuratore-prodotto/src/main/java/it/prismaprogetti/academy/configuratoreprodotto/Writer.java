package it.prismaprogetti.academy.configuratoreprodotto;

import java.io.IOException;
import java.util.List;

import it.prismaprogetti.academy.configuratoreprodotto.model.Colonna;
import it.prismaprogetti.academy.configuratoreprodotto.model.Record;

public interface Writer {
	
	public void write(List<Colonna> colonne, List<Record> records, String path) throws IOException;

}
