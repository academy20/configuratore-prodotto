package it.prismaprogetti.academy.configuratoreprodotto.model;

public class Campo {
	
	private String nome;
	private String valore;

	private Campo(String nome, String valore) {
		super();
		this.nome = nome;
		this.valore = valore;
	}

	public String getNome() {
		return nome;
	}

	public String getValore() {
		return valore;
	}

	public static Campo createOrNullString(String nome, String valore) {
		return new Campo(NullManager.createNullStringIsNecessarily(nome), NullManager.createNullStringIsNecessarily(valore));
	}
	
	public static Campo createCampoNull() {
		return new Campo(NullManager.createNullString(), NullManager.createNullString());
	}
	
	public static Campo create(String nome, String valore) {
		return new Campo(nome, valore);
	}
	
	@Override
	public int hashCode() {

		return this.nome.hashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (obj instanceof Campo) {
			Campo campo = (Campo) obj;
			return campo.getNome().equals(this.nome);
		}
		return false;
	}

}
