package it.prismaprogetti.academy.configuratoreprodotto.model;

public class Colonna {
	
	private String nome;

	private Colonna(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
	
	public static Colonna createOrNullString(String nome) {
		return new Colonna(NullManager.createNullStringIsNecessarily(nome));
	}

}
