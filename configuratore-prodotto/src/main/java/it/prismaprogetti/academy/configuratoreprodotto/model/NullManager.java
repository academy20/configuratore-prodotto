package it.prismaprogetti.academy.configuratoreprodotto.model;

public class NullManager {
	
	public static String createNullStringIsNecessarily( String param ) {
		
		if ( param.isBlank() ) {
			return "<<NULL>>";
		}
		
		return param;
	}

	public static String createNullString() {
		return "<<NULL>>";
	}
}
