package it.prismaprogetti.academy.configuratoreprodotto.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import it.prismaprogetti.academy.configuratoreprodotto.Reader;
import it.prismaprogetti.academy.configuratoreprodotto.model.Campo;
import it.prismaprogetti.academy.configuratoreprodotto.model.Record;

public class ReaderImpl implements Reader {

	private final char marcatore;

	public ReaderImpl(char marcatore) {
		super();
		this.marcatore = marcatore;
	}

	@Override
	public Record[] read(String path) throws IOException {

		List<Record> recordsList = new ArrayList<>();

		try (
				InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(path));
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			) {

			List<Campo> campi = new ArrayList<>();

			/*
			 * gestisco la prima riga del file che � dedicata alle colonne
			 */
			String intestazioneString = bufferedReader.readLine(); // CODICE,NOME,DESCRIZIONE,PREZZO,CODICE_MARCA
			String[] nomeCampiArray = intestazioneString.split(","); // [CODICE|NOME|DESCRIZIONE|PREZZO|CODICE_MARCA]

			String valoreCampo = null;// 1 dato) B001 --> 2 dato) Spaghetti Barilla 500g --> Conf.. --> 1,30 --> ecc.
			
			//String campiString = "";
			//List<String> valoriCampi = new ArrayList<>();
			Queue<String> valoriCampi = new ArrayDeque<>();
			
			int indiceRecord = 1;

			while ((valoreCampo = bufferedReader.readLine()) != null) {

				/*
				 * verifica se � la fine del record
				 */
				if (valoreCampo.contains(String.valueOf(this.marcatore))) {

					/*
					 * creo array di dati
					 */
					//String[] campiArray = campiString.split("-"); // [B001|SPAGH|ECC]
					

					/*
					 * creo i campi e li aggiungo a una lista che dovr� poi essere allegata a un
					 * record
					 */
					for (int indiceCampo = 0; indiceCampo < nomeCampiArray.length; indiceCampo++) {

						String nome = nomeCampiArray[indiceCampo];
						String valore = valoriCampi.poll();
						
						//Campo campo = Campo.create(campiArray[i]);
						Campo campo = Campo.create( nome, valore);
						
						campi.add(campo);
					}

					/*
					 * creazione del record
					 */
					Record record = new Record("record" + indiceRecord);
					recordsList.add(record);
					indiceRecord++;

					for (Campo campo : campi) {
						record.getCampiAssociati().add(campo);
					}
					campi.clear();

					
					/*
					 * passo al prossimo dato
					 */
					continue;
				}

				//campiString += valoreCampo + "-";
				valoriCampi.add(valoreCampo);
				
			}

			/*
			 * stampo intestazione
			 */
			System.out.println("//////////LETTURA FILE///////////////");
			for (int i = 0; i < nomeCampiArray.length-1; i++) {
				System.out.print(nomeCampiArray[i]+",");
			}
			System.out.println(nomeCampiArray[nomeCampiArray.length-1]);
			

			/*
			 * stampo dati
			 */
			for (Record record : recordsList) {

				for (int j = 0; j < record.getCampiAssociati().size(); j++) {

					System.out.println(record.getCampiAssociati().get(j).getNome());
				}
				
				for ( int k = 0; k < 3; k++) {
					System.out.print(marcatore);	
				}
				System.out.println();
				
			}

		}

		return recordsList.toArray(new Record[recordsList.size()]);

	}

}
