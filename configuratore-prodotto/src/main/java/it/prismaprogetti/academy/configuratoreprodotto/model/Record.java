package it.prismaprogetti.academy.configuratoreprodotto.model;

import java.util.ArrayList;
import java.util.List;

public class Record {

	private String nome;
	private List<Campo> campiAssociati;

	public Record(String nome) {
		super();
		this.nome = nome;
		this.campiAssociati = new ArrayList<>();
	}

	public String getNome() {
		return nome;
	}

	public List<Campo> getCampiAssociati() {
		return campiAssociati;
	}

	@Override
	public int hashCode() {
		return nome.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Record other = (Record) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {

		String s = "";

		for (int i = 0; i < campiAssociati.size(); i++) {

			if (i == (campiAssociati.size() - 1)) {
				s += campiAssociati.get(i).getNome();
				break;
			}

			s += campiAssociati.get(i).getNome() + ",";
		}

		return "Record [nome=" + nome + ", campiAssociati=" + s + "]";
	}

}
