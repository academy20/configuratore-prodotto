package it.prismaprogetti.academy.configuratoreprodotto.impl;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import it.prismaprogetti.academy.configuratoreprodotto.Writer;
import it.prismaprogetti.academy.configuratoreprodotto.model.Colonna;
import it.prismaprogetti.academy.configuratoreprodotto.model.Record;

public class WriterImpl implements Writer {

	private final char marcatore;

	public WriterImpl(char marcatore) {
		super();
		this.marcatore = marcatore;
	}

	@Override
	public void write(List<Colonna> colonne, List<Record> records, String path) throws IOException {

		try (
				OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(path));
				BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
			) {

			/*
			 * Inserimento intestazione nel file
			 */
			for (int i = 0; i < colonne.size(); i++) {

				/*
				 * si attiva solo all'ultimo elemento
				 */
				if (i == (colonne.size() - 1)) {

					bufferedWriter.write(colonne.get(i).getNome() + "\n");
					break;
				}

				bufferedWriter.write(colonne.get(i).getNome() + ",");

			}

			/*
			 * inserimento record nel file
			 */
			for (Record record : records) {

				/*
				 * ciclo sui campi del record e li scrivo nel file
				 */
				for (int j = 0; j < record.getCampiAssociati().size(); j++) {

					bufferedWriter.write(record.getCampiAssociati().get(j).getNome() + "\n");
				}

				/*
				 * inserimento marcatore fine record
				 */
				for (int i = 0; i < 3; i++) {
					bufferedWriter.write(marcatore);
				}
				bufferedWriter.write("\n");

			}

		}

	}

}
